package org.mobile.apptargetmobile

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.os.Handler


class SplashActivity : AppCompatActivity() {

    val SPLASH_DISPLAY_LENGHT = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(Runnable {

            var mainIntent = Intent(this, MainActivity::class.java)
            //   }
            startActivity(mainIntent)
            finish()
        }, SPLASH_DISPLAY_LENGHT.toLong())
    }
}
