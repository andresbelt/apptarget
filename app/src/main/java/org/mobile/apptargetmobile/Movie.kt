package org.mobile.apptargetmobile

data class Movie(val title: String, val posterUri: String, val overview: String)
