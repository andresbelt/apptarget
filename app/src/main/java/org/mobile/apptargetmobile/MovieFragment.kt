package org.mobile.apptargetmobile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso


class MovieFragment : Fragment() {




  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    // Creates the view controlled by the fragment
    val view = inflater.inflate(R.layout.fragment_movie, container, false)
    val titleTextView = view.findViewById<TextView>(R.id.titleTextView)
    val posterImageView = view.findViewById<ImageView>(R.id.posterImageView)
    val overviewTextView = view.findViewById<TextView>(R.id.overviewTextView)
    val args =arguments!!
    // Retrieve and display the movie data from the Bundle
    //  titleTextView.text = args.getString(MovieHelper.KEY_TITLE)

    val mitextoU = SpannableString(args.getString(MovieHelper.KEY_TITLE))
    mitextoU.setSpan(UnderlineSpan(), 0, mitextoU.length, 0)
    titleTextView.text = mitextoU

    overviewTextView.text = args.getString(MovieHelper.KEY_OVERVIEW)

    // Download the image and display it using Picasso
    if(!args.getString(MovieHelper.KEY_POSTER_URI).equals("")){
      Picasso.with(activity)
              .load(resources.getIdentifier(args.getString(MovieHelper.KEY_POSTER_URI), "drawable", activity!!.packageName))
              .into(posterImageView)
      posterImageView.visibility =View.VISIBLE;

    }

    return view
  }

  companion object {

    // Method for creating new instances of the fragment
    fun newInstance(movie: Movie): MovieFragment {

      // Store the movie data in a Bundle object
      val args = Bundle()
      args.putString(MovieHelper.KEY_TITLE, movie.title)
      args.putString(MovieHelper.KEY_POSTER_URI, movie.posterUri)
      args.putString(MovieHelper.KEY_OVERVIEW, movie.overview)

      // Create a new MovieFragment and set the Bundle as the arguments
      // to be retrieved and displayed when the view is created
      val fragment = MovieFragment()
      fragment.arguments = args
      return fragment
    }
  }

}