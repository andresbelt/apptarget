package org.mobile.apptargetmobile

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import android.content.DialogInterface
import android.app.Activity
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.util.Log
import java.util.*
import com.innoquant.moca.MOCAException
import com.innoquant.moca.MOCAUser
import com.innoquant.moca.MOCACallback
import com.innoquant.moca.MOCA
import android.support.v4.widget.DrawerLayout
import android.view.View
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), OnItemSelectedPosition, ActivityCompat.OnRequestPermissionsResultCallback {
    override fun OnItemPosition(pos: Int) {
        //  Toast.makeText(this,Int.toString(),Toast.LENGTH_LONG).show()
        viewPager.currentItem = pos
    }

    val REQUEST_PERMISSION_GEO = 101

    val TAG = "MocaSampleApp"
    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: MoviesPagerAdapter

    val permissionStr = android.Manifest.permission.ACCESS_FINE_LOCATION


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       // initLayout()

        /***************** MOCA INTEGRATION SAMPLE CODE *******************
         ****************************************************************/

        //PERMISSIONS REQUEST.
        this.requestMocaPermissions()

        //EXAMPLE: Use MOCA.login to track user properties (optional)

        //1* Get user properties from a 3rd party system, (CRM or similar)
        // Hardcoded for the occasion

        val user = Properties()
        user.setProperty("id", "7330332307333678466")
        user.setProperty("name", "Andres")
        user.setProperty("lastName", "Beltran")
        user.setProperty("birthday", "01/01/1873")
        user.setProperty("photo", "http://i.imgur.com/RmmaXxN.jpg")

        // 2* Login in MOCA with the user data
        this.mocaLogin(user)

        //REMEMBER: MOCA SDK Initialization is done in the MocaApplication Class
        /***************** END OF MOCA INTEGRATION SAMPLE CODE *************
         ****************************************************************/
        this.fetchSdkStatus()

        // Get the list of movies from the JSON file
            val movies = MovieHelper.getMoviesFromJson("movies.json", this)

        viewPager = findViewById(R.id.viewPager)

        pagerAdapter = MoviesPagerAdapter(supportFragmentManager, movies)
        viewPager.adapter = pagerAdapter



/*
        val pageIndicatorView = findViewById<PageIndicatorView>(R.id.pageIndicatorView)

   pageIndicatorView.setViewPager(viewPager)
*/

    }




    private fun fetchSdkStatus() {
        if (!MOCA.initialized()) return
        Log.d("Proximity Service: ", if (MOCA.proximityEnabled()) "RUNNING" else "STOPPED")
        Log.d("GeoLocation Service: ", if (MOCA.geoTrackingEnabled()) "RUNNING" else "STOPPED")
        Log.d("Remote Push Srv: ", if (MOCA.remotePushEnabled()) "RUNNING" else "STOPPED")
        Log.d("Event Tracking: ", if (MOCA.eventTrackingEnabled()) "RUNNING" else "STOPPED")
        Log.d("MOCA AppKey: ", MOCA.getAppKey())
        Log.d("MOCA SDK Version: ", MOCA.getVersion())


    }

    private fun mocaLogin(userProps: Properties) {
        val user = MOCA.getInstance().login(userProps.getProperty("id"))
        user?.save(object : MOCACallback<MOCAUser> {
            override fun success(mocaUser: MOCAUser) {
                Log.i(TAG, "Login successful")
            }

            override fun failure(e: MOCAException) {
                Log.e(TAG, "Login failed :-(", e)
            }
        })
    }


    private fun requestMocaPermissions() {
        if (ContextCompat.checkSelfPermission(this, permissionStr) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionStr)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                // 1. Instantiate an AlertDialog.Builder with its constructor
                val builder = AlertDialog.Builder(this)

                // 2. Chain together various setter methods to set the dialog characteristics
                val mainActivity = this
                builder.setMessage("We use location to provide you Proximity Experiences")
                        .setTitle("Optional dialog explaining permissions").setNeutralButton("ok", DialogInterface.OnClickListener { dialog, which ->
                            ActivityCompat.requestPermissions(mainActivity,
                                    arrayOf(permissionStr),
                                    REQUEST_PERMISSION_GEO)
                        }).show()

            } else {
                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        arrayOf(permissionStr),
                        REQUEST_PERMISSION_GEO)

                // REQUEST_PERMISSION_GEO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }


}

