package org.mobile.apptargetmobile

import android.app.Application
import com.innoquant.moca.MOCA

class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()

        MOCA.initializeSDK (this);
    }
}