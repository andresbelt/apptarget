package org.mobile.apptargetmobile

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class HomeFragment : Fragment() {

    private lateinit var listener: OnItemSelectedPosition


    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnItemSelectedPosition) {
            listener = context
        } else {
            throw ClassCastException(context.toString() + " must implement OnItemSelectedPosition.")

        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState:
    Bundle?): View? {

        // Creates the view controlled by the fragment
        val view = inflater.inflate(R.layout.activity_splash, container, false)
        val textOnline = view.findViewById<TextView>(R.id.textOnline)
        val textAude = view.findViewById<TextView>(R.id.textAude)
        val textCom = view.findViewById<TextView>(R.id.textCom)
        val textFid = view.findViewById<TextView>(R.id.textFid)
        val textAna = view.findViewById<TextView>(R.id.textAna)


        textOnline.setOnClickListener(View.OnClickListener {  listener.OnItemPosition(1) })
        textAude.setOnClickListener(View.OnClickListener { listener.OnItemPosition(2) })
        textCom.setOnClickListener(View.OnClickListener { listener.OnItemPosition(3) })
        textFid.setOnClickListener(View.OnClickListener { listener.OnItemPosition(4) })
        textAna.setOnClickListener(View.OnClickListener {  listener.OnItemPosition(5) })


        return view
    }

    companion object {

        // Method for creating new instances of the fragment
        fun newInstance(): HomeFragment {

            // Store the movie data in a Bundle object
            // Create a new MovieFragment and set the Bundle as the arguments
            // to be retrieved and displayed when the view is created

            return HomeFragment()
        }
    }



}

    interface OnItemSelectedPosition {
        fun OnItemPosition(pos: Int)
    }

