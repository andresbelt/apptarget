package org.mobile.apptargetmobile

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import java.util.*



// 1
class MoviesPagerAdapter(fragmentManager: FragmentManager, private val movies: ArrayList<Movie>) :
    FragmentStatePagerAdapter(fragmentManager) {

  // 2
  override fun getItem(position: Int): Fragment {
    if(position == 0){
      return HomeFragment.newInstance();}
    else
    return MovieFragment.newInstance(movies[position % movies.size])
  }

  // 3
  override fun getCount(): Int {
    return movies.size
  }

  override fun getPageTitle(position: Int): CharSequence {
    return movies[position % movies.size].title
  }
}